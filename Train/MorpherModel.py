from keras.models import Model
from keras.layers import Input, Dense, 
from keras.layers.advanced_activations import ELU

def quantile_reg_model(Inputs,nclasses,nregclasses,dropoutRate=None,batchNorm=True,qs=np.arange(0.5,0.95,0.1)):
    """quantile regression model based on the original code from N. Foppianni and O. Cerri"""
    
    custom_objects = {}
    model_pieces = []
    losses = {}
    input_layer = Input(shape = (input_len,), name='main_input')
    for q in qs:
        x = Dense(100, kernel_initializer='glorot_normal', bias_initializer='glorot_uniform')(input_layer)
        x = ELU(alpha=1.0)(x)
        x = Dense(50, kernel_initializer='glorot_normal', bias_initializer='glorot_uniform')(x)
        x = ELU(alpha=1.0)(x)
        x = Dense(25, kernel_initializer='glorot_normal', bias_initializer='glorot_uniform')(x)
        x = ELU(alpha=1.0)(x)
        x = Dense(12, kernel_initializer='glorot_normal', bias_initializer='glorot_uniform')(x)
        x = ELU(alpha=1.0)(x)
        x = Dense(3, kernel_initializer='glorot_normal', bias_initializer='glorot_uniform', name='%3.2f'%q)(x)
        model_pieces.append(x)

    return Model(inputs=[input_layer], outputs=model_pieces)
