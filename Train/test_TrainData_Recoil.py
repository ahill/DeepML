from DeepJetCore.training.training_base import training_base
from RecoilModels import *
from Losses import *
from Metrics import *
import copy

#a list of methods (name, batchNorm, dropoutRate, loss)
MODELLIST= { 
    #scale

    #non-parametric
    0:   { "method":meanDNN,                 'arch':'30x15x5',               "dropout":None, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber,    "learningrate":0.0001,  "nepochs":30, "batchsize":1024 },
    1:   { "method":meanDNN,                 'arch':'30x15x5',               "dropout":0.20, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber,    "learningrate":0.0001,  "nepochs":30, "batchsize":1024 },
    2:   { "method":meanDNN,                 'arch':'30x15x5',               "dropout":0.50, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber,    "learningrate":0.0001,  "nepochs":30, "batchsize":1024 },
    3:   { "method":meanDNN,                 'arch':'512x512x512x128x64x32', "dropout":None, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber,    "learningrate":0.0001,  "nepochs":50, "batchsize":1024 },
    4:   { "method":meanDNN,                 'arch':'512x512x512x128x64x32', "dropout":0.20, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber,    "learningrate":0.0001,  "nepochs":50, "batchsize":1024 },
    5:   { "method":meanDNN,                 'arch':'512x512x512x128x64x32', "dropout":0.50, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber,    "learningrate":0.0001,  "nepochs":50, "batchsize":1024 },
    6:   { "method":meanpquantilesDNN,       'arch':'30x15x5',               "dropout":None, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber_q,  "learningrate":0.0001,  "nepochs":30, "batchsize":1024 },
    7:   { "method":meanpquantilesDNN,       'arch':'30x15x5',               "dropout":0.20, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber_q,  "learningrate":0.0001,  "nepochs":30, "batchsize":1024 },
    8:   { "method":meanpquantilesDNN,       'arch':'30x15x5',               "dropout":0.50, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber_q,  "learningrate":0.0001,  "nepochs":30, "batchsize":1024 },
    9:   { "method":meanpquantilesDNN,       'arch':'512x512x512x128x64x32', "dropout":None, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber_q,  "learningrate":0.0001,  "nepochs":50, "batchsize":1024 },
    10:  { "method":meanpquantilesDNN,       'arch':'512x512x512x128x64x32', "dropout":0.20, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber_q,  "learningrate":0.0001,  "nepochs":50, "batchsize":1024 },
    11:  { "method":meanpquantilesDNN,       'arch':'512x512x512x128x64x32', "dropout":0.50, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":ahuber_q,  "learningrate":0.0001,  "nepochs":50, "batchsize":1024 },

    #semi-parametric
    50:  { "method":semiParamDNN,            'arch':'30x15x5',               "dropout":None, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":gd_loss,  "learningrate":0.0001,  "nepochs":50,  "batchsize":1024 },
    51:  { "method":semiParamDNN,            'arch':'30x15x5',               "dropout":0.20, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":gd_loss,  "learningrate":0.0001,  "nepochs":50,  "batchsize":1024 },
    52:  { "method":semiParamDNN,            'arch':'30x15x5',               "dropout":0.50, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":gd_loss,  "learningrate":0.0001,  "nepochs":50,  "batchsize":1024 },
    53:  { "method":semiParamDNN,            'arch':'512x512x512x128x64x32', "dropout":None, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":gd_loss,  "learningrate":0.0001,  "nepochs":100, "batchsize":1024 },
    54:  { "method":semiParamDNN,            'arch':'512x512x512x128x64x32', "dropout":0.20, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":gd_loss,  "learningrate":0.0001,  "nepochs":100, "batchsize":1024 },
    55:  { "method":semiParamDNN,            'arch':'512x512x512x128x64x32', "dropout":0.50, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":gd_loss,  "learningrate":0.0001,  "nepochs":100, "batchsize":1024 },

    #semi-parametric modified                                                                                                        
    56:  { "method":semiParamDNN,            'arch':'30x15x5',               "dropout":None, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":gd_loss_sigm_gauss,  "learningrate":0.0001,  "nepochs":50,  "batchsize":1024 },
    #semi-parametric reduced                                                                                                         
    98:  { "method":semiParamDNNreduced,            'arch':'30x15x5',               "dropout":None, "batchNorm": True,   "splitInputs": False,  'pfix':'', "loss":gd_loss_sigm_gauss_reduced,  "learningrate":0.0001,  "nepochs":50,  "batchsize":1024 },

    #direction
    100:  { "method":meanDNN,                'arch':'30x15x5',               "dropout":None, "batchNorm": True,  "splitInputs": False, 'pfix':'_e2', "loss":huber,          "learningrate":0.001, "nepochs":20, "batchsize":1024 },
    150:  { "method":semiParamDNNForE2,      'arch':'30x15x5',               "dropout":None, "batchNorm": False, "splitInputs": False, 'pfix':'',    "loss":gd_offset_loss, "learningrate":0.001, "nepochs":20, "batchsize":1024  },
    }

#start a training base class (also does the parsing)    
train=training_base(testrun=False)
nepochs=20
batchsize=1024
if  not train.modelSet():
    model=int(train.keras_model_method)
    if not model in MODELLIST:
        raise ValueError('Unknown model %d'%model)
    print 'Setting model',model
    print MODELLIST[model]

    nepochs=MODELLIST[model]['nepochs']
    batchsize=MODELLIST[model]['batchsize']
    train.setModel( model=MODELLIST[model]['method'],                    
                    arch=MODELLIST[model]['arch'],
                    dropoutRate=MODELLIST[model]['dropout'],
                    batchNorm=MODELLIST[model]['batchNorm'],
                    splitInputs=MODELLIST[model]['splitInputs'],
                    pfix=MODELLIST[model]['pfix'])

    predictionLabels=['mu']
    metricsList=['mae','mse',scale_metric]
    if MODELLIST[model]['loss']==ahuber_q:
        predictionLabels=['mu','qm','qp']
    if MODELLIST[model]['loss']==gd_loss_sigm_gauss_reduced:
        predictionLabels=['mu','sigma']
    if  MODELLIST[model]['loss']==gd_loss or MODELLIST[model]['loss']==gd_loss_sigm_gauss:
        predictionLabels=['mu','sigma','a1','a2']
    if  MODELLIST[model]['loss']==gd_offset_loss : 
        predictionLabels=['mu','sigma','a1','a2','n']
        metricsList += [dir_metric]
    predictionLabels = [x+MODELLIST[model]['pfix'] for x in predictionLabels]
    train.defineCustomPredictionLabels(predictionLabels)
    
    train.compileModel(learningrate=MODELLIST[model]['learningrate'],
                       loss=MODELLIST[model]['loss'],
                       metrics=metricsList)
    print(train.keras_model.summary())
    

model,history = train.trainModel(nepochs=nepochs, 
                                 batchsize=batchsize,
                                 stop_patience=300, #stop after N-epochs if validation loss increases
                                 lr_factor=0.5,     #adapt learning rate if validation loss flattens
                                 lr_patience=15, 
                                 lr_epsilon=0.0001, 
                                 lr_cooldown=2, 
                                 lr_minimum=0.0001, 
                                 maxqsize=100       #play if file system is unstable
                             )
