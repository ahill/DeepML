from DeepJetCore.training.training_base import training_base
from MorpherModel import quantile_reg_model
from Losses import quantile_loss_generator
import copy

#start a training base class (also does the parsing)    
train=training_base(testrun=False)
nepochs=20
batchsize=1024
train.setModel( model=quantile_loss_generator,
                dropoutRate=0,
                batchNorm=None,
                qs=np.arange(0.5,0.95,0.1) )

train.compileModel(learningrate=0.001,
                   loss=quantile_loss_generatorMODELLIST[model]['loss'],
                   metrics=['mae', 'mse'])
print(train.keras_model.summary())
    
model,history = train.trainModel(nepochs=nepochs, 
                                 batchsize=batchsize,
                                 stop_patience=300, #stop after N-epochs if validation loss increases
                                 lr_factor=0.5,     #adapt learning rate if validation loss flattens
                                 lr_patience=15, 
                                 lr_epsilon=0.0001, 
                                 lr_cooldown=2, 
                                 lr_minimum=0.0001, 
                                 maxqsize=100       #play if file system is unstable
                             )
