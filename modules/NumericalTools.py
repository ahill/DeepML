import tensorflow as tf

def rangeTransform(x,a,b,inverse=False):

    """transform to a limited range as Minuit does (cf. https://root.cern.ch/download/minuit.pdf Sec. 1.2.1)"""

    return tf.asin(2*(x-a)/(b-a)-1.0) if inverse else a+0.5*(b-a)*(tf.sin(x)+1.0)
