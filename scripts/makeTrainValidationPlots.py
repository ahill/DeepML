import json
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def parsePlotsFrom(data):
    """parse the json file and get the data"""
    x=[i+1 for i in xrange(0,len(data))]
    y={}
    for i in xrange(0,len(data)):
        epochData=data[i]
        for key in epochData.keys():
            key=str(key)
            if not key in y: y[key]=[]
            y[key].append(epochData[key])
        
    return x,y

def makeTrainValidationPlot(key,labels,xy_train,xy_val):
    """plot a figure of merit"""
    fig,ax=plt.subplots(num=key,figsize=(6,6))

    colors=['r','g','b','c','m','k']
    ymin,ymax=1e9,-1e9
    for i in xrange(0,len(labels)):
        ax.plot(xy_train[i][0], xy_train[i][1], color=colors[i], linestyle=':', label='train')
        ax.plot(xy_val[i][0],   xy_val[i][1],   color=colors[i], linestyle='-', label='validation')
        ymin=min([ymin,min(xy_val[i][1][2:-1]),min(xy_train[i][1][2:-1])])
        ymax=max([ymax,max(xy_val[i][1][2:-1]),max(xy_train[i][1][2:-1])])
    plt.xlabel('Epoch')
    plt.ylabel(key)
    plt.ylim(ymin=ymin,ymax=ymax)
    plt.legend(framealpha=0.0,loc='upper right')    
    ax.text(0,1.02,'CMS preliminary simulation', transform=ax.transAxes)
    ax.text(1.0,1.02,r'W$\rightarrow\ell\nu$ (13 TeV)', transform=ax.transAxes,horizontalalignment='right')
    for i in xrange(0,len(labels)):
        ax.text(0.73, 0.95-i*0.088, labels[i],horizontalalignment='right', transform=ax.transAxes,fontsize=8)
    fig.savefig('%s.png'%key)
    print 'Saved %s.png'%key

allKeys=[]
modelData=[]
modelList=sys.argv[1].split(',')
for model in modelList:
    data=None
    with open('%s/train/full_info.log'%model) as f:
        data=json.loads(f.read())
    modelData.append( parsePlotsFrom(data) )
    allKeys += list(modelData[-1][1].keys())

allKeys=list(set(allKeys))
for key in allKeys:
    if key.find('val')==0 : continue

    titles=[]
    xy_train=[]
    xy_val=[]
    for i in xrange(0,len(modelList)):
        if not key in modelData[i][1]: continue        
        if not 'val_'+key in modelData[i][1]: continue
        titles.append( modelList[i] )
        xy_train.append( (modelData[i][0],modelData[i][1][key]) )
        xy_val.append( (modelData[i][0],modelData[i][1]['val_'+key]) )

    makeTrainValidationPlot(key,titles,xy_train,xy_val)
    
